FROM alpine:latest as downloader

ENV S6_VERSION=v2.1.0.2
ENV S6_FILENAME s6-overlay-amd64.tar.gz
ENV S6_URL https://github.com/just-containers/s6-overlay/releases/download/${S6_VERSION}/${S6_FILENAME}

RUN wget https://github.com/Secretmapper/combustion/archive/release.zip && \
    unzip release.zip && \
    wget https://raw.githubusercontent.com/SebDanielsson/dark-combustion/master/main.77f9cffc.css && \
    wget ${S6_URL} && \
    tar xzvf ${S6_FILENAME} && \
    rm ${S6_FILENAME}

FROM alpine:latest

# maintainer
LABEL maintainer "Sebastian Danielsson <sebastian.danielsson@protonmail.com>"

# install wireguard-tools transmission-daemon
RUN apk --no-cache --virtual add wireguard-tools transmission-daemon jq 

# copy placeholder config files and startup script from host
COPY root/ .

# create volumes to load config files from host and save downloaded files to host
VOLUME ["/etc/wireguard"] \
        ["/etc/transmission-daemon"] \
        ["/transmission/complete"] \
        ["/transmission/incomplete"] \
        ["/transmission/watch"]

COPY --from=downloader /combustion-release \
    /main.77f9cffc.css \
    tmp/combustion-release/

RUN rm -rf /usr/share/transmission/web && \
    mv /tmp/combustion-release/ /usr/share/transmission/web && \
    rm -rf /tmp/combustion-release && \
    chmod +x /entrypoint.sh

# open ports, 51820 for WireGuard, 9091 for transmission-rpc
EXPOSE 51820/udp 9091

ENV PUID= \
    PGID= \
    INTERFACE=wg0 \
    KILLSWITCH= \
    S6_BEHAVIOUR_IF_STAGE2_FAILS=2

ENTRYPOINT [ "/init" ]
